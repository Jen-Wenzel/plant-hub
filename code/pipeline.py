import folium
from branca.element import Figure
from geopy.geocoders import Nominatim
import os
import json
import qrcode
import requests
from PIL import Image as PILImage
from PIL.ExifTags import GPSTAGS, TAGS
import shutil
from wand.image import Image as WImage

# ensure that image format is jpg, otherwise convert it
valid_formats = ['.jpg']
TARGET_FOLDER = '..\data\\target'
DEV_URL = 'http://localhost:5000/'

def process_img(img, id):
    checked_img = check_image(img)
    exif= get_exif(checked_img)
    geotags = get_geotagging(exif)
    coordinates = get_coordinates(geotags)
    location = reverse_geolocator(coordinates)
    species = identify_plant(checked_img)
    filename, path = change_image_name(checked_img, species, location)
    filename = os.path.split(path)[1]
    create_qr_code(filename.split('.')[0], id)
    info = save_info(species, location, filename, coordinates, path)
    return info

def save_info(species, location, filename, coordinates, path):
    lat, lon = coordinates
    plant_info = {
        'species': species, 
        'location': location,
        'lat': lat,
        'lon': lon,
        'filename': filename, 
        'path': path
    }
    return plant_info


def check_image(img): 
    input_path = img
    sf_ext = os.path.splitext(input_path)[1] # .HEIC
    sf_name = os.path.split(input_path)[1] # source.HEIC
    source_name = sf_name.split('.')[0] # source
    source_path = os.path.split(input_path)[0] # .\source\img

    if sf_ext.lower() not in valid_formats:
        with WImage(filename=input_path) as img:
            img.format = 'jpg'
            conv_img_name = f'{source_name}.{img.format.lower()}'
            conv_img_path = os.path.join(source_path, conv_img_name)
            img.save(filename=conv_img_path)
            img.close()
            print(conv_img_path)
            return conv_img_path
    else:
        return input_path

def get_exif(checked_img):
    image = PILImage.open(checked_img)
    image.verify()
    return image._getexif()

def get_geotagging(exif):
    if not exif:
        raise ValueError("No EXIF metadata found")

    geotagging = {}
    for (idx, tag) in TAGS.items():
        if tag == 'GPSInfo':
            if idx not in exif:
                raise ValueError("No EXIF geotagging found")

            for (key, val) in GPSTAGS.items():
                if key in exif[idx]:
                    geotagging[val] = exif[idx][key]

    return geotagging

def get_decimal_from_dms(dms, ref):
    degrees = dms[0]
    minutes = dms[1] / 60.0
    seconds = dms[2] / 3600.0

    if ref in ['S', 'W']:
        degrees = -degrees
        minutes = -minutes
        seconds = -seconds

    return round(degrees + minutes + seconds, 5)

def get_coordinates(geotags):
    lat = get_decimal_from_dms(geotags['GPSLatitude'], geotags['GPSLatitudeRef'])

    lon = get_decimal_from_dms(geotags['GPSLongitude'], geotags['GPSLongitudeRef'])

    return (lat,lon)

def reverse_geolocator(coordinates):

    # initialize Nominatim API (OpenStreetMap) with app name
    geolocator = Nominatim(user_agent="geovis")

    # get geo information
    location = geolocator.reverse(coordinates)
    geo_info = location.raw
    try:
        city = location.raw['address']['city']
    except:
        city = location.raw['address']['town']

    print(geo_info)
    return city

def identify_plant(checked_img):

    API_KEY = "2b10bHuob8kmpKWFrv4DoIpDO"
    API_ENDPOINT = f"https://my-api.plantnet.org/v2/identify/all?api-key={API_KEY}"

    #selected_organ = input('Leaf, Flower, Fruit or Bark?')
    selected_organ = 'auto'

    image_to_identify = open(checked_img, 'rb')

    data = {
            'organs': [selected_organ.lower()]
    }

    files = [
            ('images', (checked_img, image_to_identify))
    ]

    try:
        req = requests.Request('POST', url=API_ENDPOINT, files=files, data=data)
        prepared_req = req.prepare()

        s = requests.Session()
        r = s.send(prepared_req)
        json_result = json.loads(r.text)

        species_name = json_result['results'][0]['species']['scientificNameWithoutAuthor']

    except:
        print('Plant could not be identified.')

    print(r.status_code)
    print(json_result)
    
    image_to_identify.close()
    return species_name

def transform_name(name):
    # normalize names to create file name

    '''
    if string has whitespaces -> replace with underscore
    if string is longer than 15 char -> trim and replace last char with an asterisk 
    '''
    norm_name = normalize_name(name)
    trimmed_name = trim_name(norm_name)
    return trimmed_name

def normalize_name(name):
    if ' ' in name:
        norm_name = name.replace(' ', '_')
        return norm_name
    else:
        return name

def trim_name(name):
    if len(name) >= 25:
        trim_name = ''.join([name[:25], 'X'])
        return trim_name
    else:
        print(name)
        return name

def change_image_name(img, species, location):
    name_species = transform_name(species)
    name_city = transform_name(location)

    with WImage(filename=img) as img:
                img.format = 'jpg'
                final_fname = (''.join([name_species, '-', name_city, '.', img.format.lower()]))
                final_path = os.path.join(TARGET_FOLDER, final_fname)
                img.save(filename = final_path)
                print(final_path)
                img.close()
                return (final_fname, final_path)

def create_qr_code(name, id):
    # create QR code to tag plant
    
    img = qrcode.make(f'{DEV_URL}{id+1}')
    type(img)  # qrcode.image.pil.PilImage
    qr_name = f'QR_{name}.png'
    qr_path = os.path.join(TARGET_FOLDER, qr_name)
    img.save(qr_path)