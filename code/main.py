import folium
from branca.element import Figure
from flask import Flask, flash, request, render_template, redirect, url_for, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import os
from werkzeug.utils import secure_filename

from pipeline import process_img

app = Flask(__name__)
project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "plants.db"))
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


UPLOAD_FOLDER = '..\data\source'
TARGET_FOLDER = '..\data\\target'
ALLOWED_EXTENSIONS = {'heic', 'png', 'jpg', 'jpeg', 'gif'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['TARGET_FOLDER'] = TARGET_FOLDER


class Plant(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    species = db.Column(db.Text)
    location = db.Column(db.Text)
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    filename = db.Column(db.Text)
    path = db.Column(db.Text)
    date_added = db.Column(db.DateTime, default=datetime.now())


def create_entry(species, location, lat, lon, filename, path):
    plant = Plant(species=species, location=location, lat=lat, lon=lon, filename=filename, path=path)
    db.session.add(plant)
    db.session.commit()
    db.session.refresh(plant)


def get_entries():
    return db.session.query(Plant).all()

def update_entry(plant_id, delete):
    db.session.query(Plant).filter_by(id=plant_id).update({
        "delete": True if delete == "on" else False
    })
    db.session.commit()

def delete_entry(plant_id):
    db.session.query(Plant).filter_by(id=plant_id).delete()
    db.session.commit()

@app.route("/", methods=["POST", "GET"])
def view_index():
    if request.method == "POST":
        upload_file()
    
    fig = Figure(width=1100,height=350)
    map = folium.Map(width=1100,height=350, location=(52.47716, 13.42259), zoom_start=7)
    fig.add_child(map)

    tile = folium.TileLayer(
        tiles = 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        attr = 'Esri',
        name = 'Esri Satellite',
        overlay = False,
        control = True
       ).add_to(map)

    plants = get_entries()
    for plant in plants:
        coordinates = (plant.lat, plant.lon)
        folium.Marker(
        location=coordinates,
        popup = folium.Popup(plant.species, max_width=500), 
        tooltip = plant.species,
        icon = folium.features.CustomIcon(plant.path, 
        icon_size=(70, 70))).add_to(map)
    map.save('templates/map.html')
        
    return render_template("index.html", plants=get_entries())

@app.route("/<plant_id>", methods=["GET"])
def get_single_entry(plant_id):
    plant = db.session.query(Plant).get(plant_id)
    return render_template("single_plant.html", plant=plant)

@app.route("/edit/<plant_id>", methods=["POST", "GET"])
def edit_entry(plant_id):
    if request.method == "POST":
        update_entry(plant_id, delete=request.form['delete'])
    elif request.method == "GET":
        delete_entry(plant_id)
    return redirect("/", code=302)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)

            last_plant = db.session.query(Plant).order_by(-Plant.id).first()
            print(last_plant.id)
            plant_info = process_img(file_path, last_plant.id)
            create_entry(plant_info['species'], plant_info['location'], plant_info['lat'], plant_info['lon'], plant_info['filename'], plant_info['path'])
            return redirect(url_for('download_file', name=plant_info['filename']))
    
@app.route('/uploads/<name>')
def download_file(name):
    return send_from_directory(app.config["TARGET_FOLDER"], name)

@app.route('/map')
def map():
    return render_template('map.html')


if __name__ == "__main__":
    db.create_all()
    app.run(host='localhost', port=5000, debug=True)